import sqlite3

class SQL:
  def __init__(self, fp):
    self.__fp = fp
    self.__db = None

  @property
  def fp(self):
    return self.__fp

  @property
  def db(self):
    return self.__db

  def connect(self):
    self.__db = sqlite3.connect(self.__fp)

  def create(self, name):
    self.db.execute("""
        CREATE TABLE {} (
            id INTEGER
            , marke varchar(50)
            , kennzeichen varchar(10)
            , fahrgestellnr TEXT
            , baujahr INTEGER
            , erstzulassung INTEGER
            , PRIMARY KEY(id))""".format(name))

  def insert(self, data):
    for d in data:
      self.db.execute("""
          INSERT INTO fahrtenbuch (marke, kennzeichen, fahrgestellnr, baujahr, erstzulassung, id_)
          VALUES (?, ?, ?, ?, ?, ?);""", d)
