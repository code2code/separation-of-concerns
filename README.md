Was bedeutet Separation of Concerns?
==

Das Prinzip ist einfach: Separation of Concerns (Trennung der
Verantwortlichkeiten) bedeutet, dass man ein Programm nicht in einem
monolithischen Block erstellt, sondern in kleine funktionalle Einheiten
auftrennt, die eine einzige **dedidizierte** Aufgabe haben.

Das Prinzip habt ihr bereits kennen gelernt und angewandt. Zuerst in der
prozeduralen Programmierung und später bei der Aufteilung in mehrere Module.

Heute wollen wir uns zusätzlich noch ansehen, wie man in Python kleine
Programmpakete erstellt, wie z.B. das `requests` Paket oder `json`.

Wann trenne ich meinen Code auf?
===

Funktionen
====

Wie bereits bekannt sein dürfte, ist das Hauptkriterium um eine Funktion in
mehrere Teilfunktionen aufzustrennen deren Länge bzw die Tatsache, das in der
Funktion zuviele Dinge auf einmal geschehen. Ein einfaches Beispiel:

```python
def fetch_data_and_send_to_sql(url):
  response = requests.get(url)
  data = response.json()
  # evtl noch data processing z.B. aus json umwandeln, dict anlegen / auslesen, etc
  # ...

  db = sqlite3.connect('fahrzeugverwaltung.db')

  db.execute("""
      CREATE TABLE fahrzeuge (
          id INTEGER
          , marke varchar(50)
          , kennzeichen varchar(10)
          , fahrgestellnr TEXT
          , baujahr INTEGER
          , erstzulassung INTEGER
          , PRIMARY KEY(id))""")


  for d in data:
    db.execute("""
        INSERT INTO fahrtenbuch (marke, kennzeichen, fahrgestellnr, baujahr, erstzulassung, id_)
        VALUES (?, ?, ?, ?, ?, ?);""", d)
```

Wie könnte ich nun diese Funktion auftrennen?

Zum Beispiel so:

Eine Funktion um die Daten abzuholen:

```python
def fetch_data(url):
  response = requests.get(url)
  data = response.json()
  # evtl noch data processing z.B. aus json umwandeln, dict anlegen / auslesen, etc
  # ...
  return data
```

Eine Funktion für die Verbindung zur Datenbank:

```python
  def connect_db(fp):
    return sqlite3.connect(db)
```

Eine Funktion um die Tabelle anzulegen:

```python
  def create_fahrzeuge(db):
    db.execute("""
        CREATE TABLE fahrzeuge (
            id INTEGER
            , marke varchar(50)
            , kennzeichen varchar(10)
            , fahrgestellnr TEXT
            , baujahr INTEGER
            , erstzulassung INTEGER
            , PRIMARY KEY(id))""")
```

Eine Funktion um die Tabelle anzulegen:

```python
  def insert_data(db, data):
    for d in data:
      db.execute("""
          INSERT INTO fahrtenbuch (marke, kennzeichen, fahrgestellnr, baujahr, erstzulassung, id_)
          VALUES (?, ?, ?, ?, ?, ?);""", d)
```

Und schlussendlich die Funktion in der wir alles zusammen fügen:

```python
def fetch_data_and_send_to_sql(url):
  data = fetch_data(url)
  db = connect_db('fahrzeuge.db')
  create_fahrzeuge(db)
  insert_data(db, data)
```

Vorteile
====

* Übersichtlichkeit: Der Code in der letzten Funktion ist deutlich
  übersichtlicher und nachvollziehbarer

* Wartbarkeit: Die einzelnen Teilfunktionen lassen sich einfacher auf Fehler
  prüfen und debuggen

* Testbarkeit: Einzelne Teilfunktionen lassen sich wesentlich einfacher auf ihre
  Funktionalität testen

* Wiederverwendbarkeit: Teilfunktionen können nun auch einfach in anderen
  Programmteilen wieder verwendet werden, ohne Copy & Paste

* Schnellere Entwicklung: Durch die Aufteilung ist es einfacher den Code
  anzupassen und Teile des Codes umzuschreiben bzw zu verbessern

* Vereinfachte Zusammenarbeit: Mehrere Entwickler können einfacher zusammen
  arbeiten, in dem sie an getrennten Funktionen arbeiten.

Analogie zu Modulen und Paketen
===

Diese Vorteile fallen zusammen unter dem Stichpunkten Loose Coupling (Lose
Bindung) und High Cohesion (hohe Kohäsion). Das bedeutet, das die funktionallen
Einheiten wenig bis gar keine Berührungspunkte haben (Bindung) und sich leicht
nach dem Grad ihrer Zusammengehörigkeit gruppieren lassen.

Dieses Prinzip habt ihr bereits durch die Modularisierung und Objektorientierung
kennen gelernt. Die oben genannten Vorteile gelten dabei nicht nur für
Funktionen, sondern ganz generell auch für Objekte, Module und Pakete.

Module
====

Module trennen ein Programm in logisch separate Einheiten, welche sich
kontextuell aber immer noch Programmspezifisch zuordnen lassen. Um das oben
genannte Beispiel fortzuführen:

Modul `data.py`:

```python
class Data:
  def __init__(self, url):
    self.__url = url
    self.__data = None

  @property
  def data(self):
    return data

  def fetch(self):
    response = requests.get(self.__url)
    data = response.json()
    # evtl noch data processing z.B. aus json umwandeln, dict anlegen / auslesen, etc
    # ...
    self.__data = data
```

Modul `sql.py`:

```python
  class SQL:
    def __init__(self, fp):
      self.__fp = fp
      self.__db = db

    @property
    def db(self):
      return self.__db

    def connect(self):
      self.__db = sqlite3.connect(self.__fp)

    def create(self, name):
      self.db.execute("""
          CREATE TABLE {} (
              id INTEGER
              , marke varchar(50)
              , kennzeichen varchar(10)
              , fahrgestellnr TEXT
              , baujahr INTEGER
              , erstzulassung INTEGER
              , PRIMARY KEY(id))""".format(name))

    def insert(self, data):
      for d in data:
        self.db.execute("""
            INSERT INTO fahrtenbuch (marke, kennzeichen, fahrgestellnr, baujahr, erstzulassung, id_)
            VALUES (?, ?, ?, ?, ?, ?);""", d)
```

Modul `main.py`:

```python
  from sql import SQL
  from data import Data

  def main():
    d = Data(url)

    s = SQL('fahrzeuge.db')
    s.connect()
    s.create('fahrzeuge')

    d.fetch()
    s.insert(d.data)
```

Pakete
====

Lässt sich eine Funktionalität Abstrahieren und Generalisieren, d.h. wenn sie in
einem übergeordneten Kontext nicht mehr einem einzelenen Programm zugeordnet
werden kann, dann bietet es sich an ein Paket zu erstellen.

Um die Anfangsbeispiele `requests` und `json` zu bemühen, ist hier deutlich,
dass die Abfrage von Daten von einem Server eine sehr generelle Aufgabe ist, die
sich in tausenden von Programmen wieder findet. Genau dasselbe gilt auch für die
Wandlung und Formatierung von Daten von und nach JSON.

Um bei unserem Beispiel zu bleiben: Das sql Modul könnte einen Mehrwert als
Paket bieten, wenn ich z.B. auf dieselbe Datenbank von mehreren Programmen aus
zugreifen möchte.

Nutzung von Paketen
=====

Pakete lassen sich sehr einfach mit `pip` verwalten:

```bash
$ pip install fancy_pkg
$ pip uninstall fancy_pkg
```

Es ist von Vorteil hierfür ein so genanntes Virtual Environment (`venv`) zu
nutzen:

```bash
$ pip install venv
$ cd /my/project/folder
$ python3 -m venv env
$ source env/bin/activate
```

Paketierung
=====

Wie lässt sich nun ein Python Programm paketieren?

Anleitung:

> https://packaging.python.org/tutorials/packaging-projects/
